#!/bin/bash
#
# description: This shell script generates preinsert tube information to preinserttubes.csv
#
# arguments: areaType rackNo rackCode tubeCount
# 
#            管架位类型，必选项
#                 0-轨道
#                 1-IO模块装载区
#                 2-IO模块卸载区
#                 3-IO模块检测区
#                 4-离心模块缓存区
#                 5-离心模块加工区
#                 6-离心模块配平区
#            管架位编号，必选项>0的整数
#            管架编号，可选项，以N、E、L开头的自定义字符
#                 N-常规50孔架
#                 E-急诊50孔管架
#                 L-常规21孔管架
#            预插入试管位编号，可选项，>0的整数，支持设置多个，以空格隔开
#
# usage: ./preinserttubes.sh tubeCount areaType rackNo rackCode [preinsertPos] [writeMode]
#

# 常规架上试管位置枚举（屏蔽最外圈）
tubeposOfRackN=(7 8 9 12 13 14 17 18 19 22 23 24 27 28 29 32 33 34 37 38 39 42 43 44)
tubeposOfRackN_len=${#tubeposOfRackN[@]}

# 判断参数完整性，至少要有tubeCount这1个参数
if [[ $# -lt 1 || $# -gt 6 ]]; then
    echo "Usage: $0 tubeCount areaType rackNo rackCode [preinsertPos] [writeMode]"
    echo "Argments:"
    echo "    tubeCount:            int number, can't be large than the tube count of one rack"
    echo "    areaType:             only support enum numbers (0 1 2 3 4 5 6), default: 2"
    echo "                          0-轨道"
    echo "                          1-IO模块装载区"
    echo "                          2-IO模块卸载区"
    echo "                          3-IO模块检测区"
    echo "                          4-离心模块缓存区"
    echo "                          5-离心模块加工区"
    echo "                          6-离心模块配平区"
    echo "    rackNo:               int number, default: 1"
    echo "    rackCode:             begin with N E L, default: N0001"
    echo "                          N-常规50孔架"
    echo "                          E-急诊50孔管架"
    echo "                          L-常规21孔管架"
    echo "    preinsertPos:         int numbers, support more than one number split by ','"
    echo "    writeMode:            write file mode, default: 1"
    echo "                          1-覆盖"
    echo "                          2-追加"
    exit 1
fi
# 判断试管个数有效性
if [[ ! "$1" =~ ^[0-9]+$ ]]; then
    echo "argument is invalid: [tubeCount]"
    exit 1
else
    tubeCnt=$1
fi
# 判断管架位类型有效性
if [ $# -lt 2 ]; then
    areaType=2
elif [[ $2 -ne 0 && $3 -ne 1 && $3 -ne 2 && $3 -ne 3 && $3 -ne 4 && $3 -ne 5 && $3 -ne 6 ]]; then
    echo "argument is invalid: [areaType]"
    exit 1
else
    areaType=$2
fi
# 判断管架位编号参数有效性
if [ $# -lt 3 ]; then
    rackNo=1
elif [[ ! "$3" =~ ^[0-9]+$ ]]; then
    echo "argument is invalid: [rackNo]"
    exit 1
else
    rackNo=$3
fi
# 判断管架编号参数有效性
if [ $# -lt 4 ]; then
    rackCode="N0001"
elif [[ ! "$4" =~ ^[NEL]{1}[0-9]+$ ]]; then
    echo "argument is invalid: [rackCode]"
    exit 1
else
    rackCode=$4
fi
# 判断预插入试管位编号参数有效性
if [ $# -lt 5 ]; then
    preinsertpos=""
else
    # 将所有的,替换成空格
    preinsertpos=$5
    preinsertpos=${preinsertpos//','/' '}
fi
# 判断写入模式参数有效性
if [ $# -lt 6 ]; then
    writeMode=1
elif [[ $6 -ne 1 && $6 -ne 2 ]]; then
    echo "argument is invalid: [writeMode]"
    exit 1
else
    writeMode=$6
fi

# 写文件
if [[ $writeMode -eq 1 ]]; then
    echo "管架位类型,管架位编号,管架编号,预插入试管位编号" > preinserttubes.csv
fi
# 判断用户是否输入preinsertpos，-n表示非空
if [[ -n "$preinsertpos" ]]; then
    echo $areaType","$rackNo","$rackCode","$preinsertpos >> preinserttubes.csv
else
    if [[ "$4" =~ ^N[0-9]+$ && $tubeCnt -gt $tubeposOfRackN_len ]]; then
        echo "argument is invalid: tubeCount > $tubeposOfRackN_len"
        exit 1
    else
        let posloopCount=$[tubeCnt - 1]
        for i in `seq 0 1 $posloopCount`
        do
            echo $areaType","$rackNo","$rackCode","${tubeposOfRackN[$i]} >> preinserttubes.csv
        done
    fi
fi
echo "write tube info into preinserttubes.csv successfully"