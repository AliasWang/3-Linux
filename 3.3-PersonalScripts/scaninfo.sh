#!/bin/bash
#
# description: This shell script generates sample information to scaninfo.csv
#
# arguments: startSampleid sampleCount [cupType] [existCap] [isCentrifuged] [bloodVolume]  
#
#            enum ECupType	
#            {	
#                eCupType_Unknow = 0	
#                eCupType_NoCup = 1	                             // 无容器
#                eCupType_13x75mm = 2	                           // 13x75mm样本管
#                eCupType_13x100mm = 3	                          // 13x100mm样本管
#                eCupType_16x75mm = 4	                           // 16x75mm样本管
#                eCupType_16x100mm = 5	                          // 16x100mm样本管
#                eCupType_Hitachi_2ml = 6	                       // 日立2mL微量杯
#                eCupType_Beckman_2ml = 7	                       // 贝克曼2mL微量杯
#                eCupType_Beckman_halfml = 8	                    // 贝克曼0.5mL微量杯
#                eCupType_Bullet = 9	                            // 子弹头
#                eCupType_Bullet_13x75mm = 10	                   // 子弹头+13x75mm
#                eCupType_Bullet_Hitachi_2ml = 11	               // 子弹头+日立2ml
#                eCupType_Bullet_13x100mm = 12	                  // 子弹头+13x100mm
#                eCupType_Kangjian_QC = 13	                      // 康健质控瓶
#                eCupType_Mindray_QC = 14	                       // 迈瑞质控瓶
#                eCupType_AbbottMicro_13x100mm = 15	             // 雅培微量杯+13x100mm样本管
#                eCupType_AbbottMicro = 16	                      // 雅培微量杯
#                eCupType_Centrifuge_2ml = 17	                   // 2ml离心管
#                eCupType_Centrifuge_5ml = 18	                   // 5ml离心管
#                eCupType_GongdongMicro_13x75mm = 19	            // 拱东微量杯+Φ13x75mm样本管
#                eCupType_GongdongMicro_13x100mm = 20	           // 拱东微量杯+Φ13x100mm样本管
#                eCupType_Flexible_13x75mm_Tube = 21	            // 微量软管+Φ13x75mm样本管
#                eCupType_Flexible_13x100mm_Tube = 22	           // 微量软管+Φ13x100mm样本管
#                eCupType_Adapter_A = 23	                        // 适配器A
#                eCupType_Adapter_A_Kangjian_QC = 24	            // 适配器A+康健质控瓶
#                eCupType_Adapter_A_Hitachi_2ml = 25	            // 适配器A+日立2ml
#                eCupType_Adapter_A_Bullet = 26	                 // 适配器A+子弹头
#                eCupType_Adapter_A_Bullet_Hitachi_2ml = 27	     // 适配器A+子弹头+日立2ml
#                eCupType_Adapter_A_Beckman_2ml = 28	            // 适配器A+贝克曼2ml
#                eCupType_Adapter_A_Beckman_halfml = 29	         // 适配器A+贝克曼0.5ml
#                eCupType_Adapter_B = 30	                        // 适配器B
#                eCupType_Adapter_B_Mindray_QC = 31	             // 适配器B+迈瑞质控瓶
#                eCupType_Adapter_B_Kangjian_QC = 32	            // 适配器B+康健质控瓶
#                eCupType_Adapter_B_Hitachi_2ml = 33	            // 适配器B+日立2ml
#                eCupType_Adapter_B_Bullet_Hitachi_2ml = 34	     // 适配器B+子弹头+日立2ml
#                eCupType_Adapter_B_Beckman_2ml = 35	            // 适配器B+贝克曼2ml
#                eCupType_Adapter_B_Beckman_halfml = 36	         // 适配器B+贝克曼0.5ml
#            }
#
# usage: ./scaninfo.sh startSampleid sampleCount
#        ./scaninfo.sh startSampleid sampleCount [cupType] [existCap] [isCentrifuged] [bloodVolume]

# cupType枚举
cupTypes=(2 3 4 5 19 20)
# cupHeight枚举
cupHeights=()
cupHeights[2]=75
cupHeights[3]=100
cupHeights[4]=75
cupHeights[5]=100
cupHeights[19]=75
cupHeights[20]=100
# cupDiameter枚举
cupDiameters=()
cupDiameters[2]=13
cupDiameters[3]=13
cupDiameters[4]=16
cupDiameters[5]=16
cupDiameters[19]=13
cupDiameters[20]=13
# existCap枚举
existCaps=(0 1)
# isCentrifuged枚举
isCentrifugeds=(0 1)

# 判断参数完整性，至少要有startSampleid和sampleCount这两个参数
if [[ $# -lt 2 || $# -gt 6 ]]; then
    echo "Usage: $0 startSampleid sampleCount [cupType] [existCap] [isCentrifuged] [bloodVolume]"
    echo "Argments:"
    echo "    startSampleid:        end with digits"
    echo "    sampleCount:          int number"
    echo "    cupType:              only support enum numbers (2 3 4 5 19 20 -1), default: 2"
    echo "                          eCupType_13x75mm = 2	                           // 13x75mm样本管"
    echo "                          eCupType_13x100mm = 3	                           // 13x100mm样本管"
    echo "                          eCupType_16x75mm = 4	                           // 16x75mm样本管"
    echo "                          eCupType_16x100mm = 5	                           // 16x100mm样本管"
    echo "                          eCupType_GongdongMicro_13x75mm = 19	           // 拱东微量杯+Φ13x75mm样本管"
    echo "                          eCupType_GongdongMicro_13x100mm = 20	           // 拱东微量杯+Φ13x100mm样本管"
    echo "                          random of (2 3 4 5 19 20) = -1	           // 随机一种样本管"
    echo "    existCap:             only support enum numbers (0 1 -1), default: 1"
    echo "                          0(No Cap)"
    echo "                          1(Have Cap, default)"
    echo "                          -1(Random of 0 and 1)"
    echo "    isCentrifuged:        only support enum numbers (0 1 -1), default: 0"
    echo "                          0(No Centrifuged, default)"
    echo "                          1(Is Centrifuged)"
    echo "                          -1(Random of 0 and 1)"
    echo "    bloodVolume:          float number, default: 3.3"
    exit 1
fi
# 判断样本个数有效性
if [[ ! "$2" =~ ^[0-9]+$ ]]; then
    echo "argument is invalid: [sampleCount]"
    exit 1
else
    sampleCnt=$2
fi
# 判断样本管类型有效性
if [ $# -lt 3 ]; then
    cupType=2
elif [[ $3 -ne 2 && $3 -ne 3 && $3 -ne 4 && $3 -ne 5 && $3 -ne 19 && $3 -ne 20 && $3 -ne -1 ]]; then
    echo "argument is invalid: [cupType]"
    exit 1
else
    cupType=$3
fi
# 判断是否有盖参数有效性
if [ $# -lt 4 ]; then
    existCap=1
elif [[ $4 -ne 0 && $4 -ne 1 && $4 -ne -1 ]]; then
    echo "argument is invalid: [existCap]"
    exit 1
else
    existCap=$4
fi
# 判断是否已离心参数有效性
if [ $# -lt 5 ]; then
    isCentrifuged=0
elif [[ $5 -ne 0 && $5 -ne 1 && $5 -ne -1 ]]; then
    echo "argument is invalid: [isCentrifuged]"
    exit 1
else
    isCentrifuged=$5
fi
# 判断血量参数有效性
if [ $# -lt 6 ]; then
    bloodVolume=3.3
elif [[ ! "$6" =~ ^[0-9.]+$ ]]; then
    echo "argument is invalid: [bloodVolume]"
    exit 1
else
    bloodVolume=$6
fi

# 递增样本编号
GetIncreasedSampleIds()
{
    # 起始编号
    startid=$1
    # 递增个数
    samplecount=$2
    # 定义一个数组用来存放递增后的样本编号
    sampleid_arr=()
    #echo $startid
    totallen=${#startid}
    #echo $totallen
    numlen=0
    for i in `seq $totallen -1 1`
    do
        #echo $i
        ch=`expr substr "$startid" $i 1`
        #echo $ch
        # 判断是否是数字
        if [[ "$ch" =~ ^[0-9]$ ]]; then
            let numlen=$[numlen + 1]
        else
            break
        fi
    done
    # 分割前缀和数值部分
    let prefixlen=$[totallen - numlen]
    let numindex=$[totallen - numlen + 1]
    prefix=`expr substr "$startid" 1 $prefixlen`
    startindex=`expr substr "$startid" $numindex $numlen`
    #echo $prefix
    #echo $startindex
    # 递增编号
    for offset in `seq $samplecount`
    do
        let incresedid=$[startindex + offset - 1]
        # 补齐0
        format="%0"$numlen"d"
        incresedid=$(printf $format $incresedid)
        # 前缀和递增的数字拼接
        #echo $prefix$incresedid
        sampleid_arr[$offset]=$prefix$incresedid
    done
    echo ${sampleid_arr[*]}
}

# 递增编号
sampleids=($(GetIncreasedSampleIds $1 $2))

# 写文件
echo "barcode,cupType,cupHeight,cupDiameter,existCap,isCentrifuged,bloodVolume" > scaninfo.csv
let loopCount=$[sampleCnt - 1]
for i in `seq 0 1 $loopCount`
do
    # 样本管类型、高度、直径
    if [[ $3 -eq -1 ]]; then
        cupType_len=${#cupTypes[@]}
        cupType_index=$(( RANDOM % cupType_len ))
        cupType=${cupTypes[$cupType_index]}
    fi
    cupHeight=${cupHeights[$cupType]}
    cupDiameter=${cupDiameters[$cupType]}
    # 是否有盖
    if [[ $4 -eq -1 ]]; then
        existCap=$(( RANDOM % 2 ))
    fi
    # 是否已离心
    if [[ $5 -eq -1 ]]; then
        isCentrifuged=$(( RANDOM % 2 ))
    fi
    echo ${sampleids[$i]}","$cupType","$cupHeight","$cupDiameter","$existCap","$isCentrifuged","$bloodVolume >> scaninfo.csv
done
echo "write sample info into scaninfo.csv successfully"