#!/bin/bash

: <<'COMMENT'
 * @Author: wangsheng 
 * @Date: 2019-10-02 13:39:38 
 * @Last Modified by:   wangsheng 
 * @Last Modified time: 2019-10-02 13:39:38 
COMMENT

# https://www.toutiao.com/i6742274742071329288/
# Shell脚本100例：33 使用RANDOM变量生成随机数并提取最大值

# 使用declare声明变量为整数
declare -i MAX=0
for i in `seq 1 10`
do
    # $RANDOM生成随机数
    MYRAND=$RANDOM
    if [ $i -le 9 ]
    then
        # echo -n 不进行换行
        echo -n "$MYRAND,"
    else
        echo $MYRAND
    fi
    # 使用条件判断取最大值
    [ $MYRAND -gt $MAX ] && MAX=$MYRAND
done
echo "Then Max is $MAX"