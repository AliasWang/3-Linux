#!/bin/bash

: <<'COMMENT'
 * @Author: wangsheng 
 * @Date: 2019-10-02 01:38:21 
 * @Last Modified by:   wangsheng 
 * @Last Modified time: 2019-10-02 01:38:21 
COMMENT

# https://www.toutiao.com/i6742099708027601416/
# Shell脚本100例：32 压缩并归档文件

read -p "请输入要归档的三个文件: " FILE1 FILE2 FILE3
read -p "请输入归档后的文件名称 " DEST
read -p "请选择压缩类型 gzip|bzip2|xz: " YASUO
 
case $YASUO in
gzip)
    tar -zcvPf ${DEST}.tar.gz $FILE1 $FILE2 $FILE3
    ;;
bzip2)
    tar -jcvPf ${DEST}.tar.bz2 $FILE1 $FILE2 $FILE3
    ;;
xz)
    tar -JcvPf ${DEST}.tar.xz $FILE1 $FILE2 $FILE3
    ;;
*)
    echo "Unknown"
    exit 9
    ;;
esac