#!/bin/bash

: <<'COMMENT'
 * @Author: wangsheng 
 * @Date: 2019-10-02 00:33:45 
 * @Last Modified by:   wangsheng 
 * @Last Modified time: 2019-10-02 00:33:45 
COMMENT

# https://www.toutiao.com/i6742034795708023309/
# Shell脚本100例：31 使用case、while和read进行查看磁盘信息

cat << EOF
    D|d 显示硬盘信息
    M|m 显示内存信息
    S|s 显示交换分区信息
    E|e 退出
EOF
 
read -p "请输入以上对应参数:" CANSHU
while [ $CANSHU != "E" ] && [ $CANSHU != "e" ]
do
    case $CANSHU in
    d|D)
        df -h
        ;;
    m|M)
        free -m | grep "内存"
        ;;
    s|S)
        free -m | grep "交换"
        ;;
    *)
        echo "Unknown"
        ;;
    esac
    read -p "请再次输入以上对应参数:" CANSHU
done