#!/bin/bash

: <<'COMMENT'
 * @Author: wangsheng 
 * @Date: 2019-10-02 23:26:21 
 * @Last Modified by:   wangsheng 
 * @Last Modified time: 2019-10-02 23:26:21 
COMMENT

# https://www.toutiao.com/a6742279542351593991/
# Shell脚本100例：34 使用for循环和if语句批量新建/删除用户

if [ $# -lt 1 ]
then
    echo "Usage: useradd ARG"
    exit 2
fi
# 新建用户
if [ $1 = 'add' ]
then
    for i in `seq 1 10`
    do
        id user$i &> /dev/null
        if [ $? -ne 0 ]
        then
            useradd -M user$i
            echo "user$i" | passwd --stdin user$i &> /dev/null
            echo "user$i create success"
        else
            echo "user$i exist"
        fi
    done
# 删除用户
elif [ $1 = 'del' ]
then
    for i in `seq 1 10`
    do
        if id user$i &> /dev/null
        then 
            userdel user$i
            echo "user$i delete success"
        else
            echo "NO such user$i"
        fi
    done
else
    echo "Please checck your input ARGS"
    exit 1
fi